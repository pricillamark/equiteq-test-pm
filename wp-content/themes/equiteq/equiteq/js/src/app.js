console.log('Hello Equiteq');

//toggle
document.addEventListener('DOMContentLoaded', function () {
    var dropdownLabels = document.querySelectorAll('.dropdown-label');
    dropdownLabels.forEach(function (dropdownLabel) {
        dropdownLabel.addEventListener('click', function () {
            var dropdownList = this.nextElementSibling;
            dropdownList.classList.toggle('show');
        });
    });

    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches('.dropdown-label')) {
            var dropdowns = document.getElementsByClassName("typeListing");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
});