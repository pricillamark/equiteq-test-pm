<?php
/**
 *  Template Name: Team
 *
 */

 get_header(); ?>
<div class="our-team-container">
    <div class="container">
        <div class="page-center">
            <div class="title-text">
                <h3>MEET OUR EXPERTS</h3>
                <p class="title-subtext">Deep knowledge, surprising insights, and a collaborative approach to growing and selling your knowledge-based business. Meet the team that has made Equiteq the leading global investment bank for the knowledge economy.</p>
            </div>
            <div class="row our-team-filters">
                <div class="col-md-6 filters-left">
                    <div class="title">
                        <h5>
                            FILTERS
                        </h5>
                    </div>
                    <div class="row main-filter-box">
                        <div class="col-md-6 sector-filter filterbox">
                            <label id="typeListLabelSector" class="dropdown-label"><span>Sector</span> <i class="fa fa-caret-down"></i></label>
                            <ul data-id="type" class="typeListing" data-filter-group="sector">
                                <li data-filter="" data-value="all" class="selected" data-search="All">
                                    <a href="javascript:void(0)">All</a></li>
                                <li data-filter=".technology-services---outsourcing" data-value="1" data-category="technology_services_-_outsourcing" data-search="Technology Services &amp; Outsourcing"><a href="javascript:void(0)">Technology Services &amp; Outsourcing</a></li>
                                <li data-filter=".software" data-value="2" data-category="software" data-search="Software"><a href="javascript:void(0)">Software</a></li>
                                <li data-filter=".management-consulting" data-value="3" data-category="management_consulting" data-search="Management Consulting"><a href="javascript:void(0)">Management Consulting</a></li>
                                <li data-filter=".engineering-consulting---services" data-value="4" data-category="engineering_consulting_-_services" data-search="Engineering Consulting &amp; Services"><a href="javascript:void(0)">Engineering Consulting &amp; Services</a></li>
                                <li data-filter=".human-capital-management" data-value="5" data-category="human_capital_management" data-search="Human Capital Management"><a href="javascript:void(0)">Human Capital Management</a></li>
                                <li data-filter=".marketing--communications---information-services" data-value="6" data-category="marketing-_communications_-_information_services" data-search="Marketing, Communications &amp; Information Services"><a href="javascript:void(0)">Marketing, Communications &amp; Information Services</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 location-filter filterbox">
                            <label id="typeListLabelLocation" class="dropdown-label"><span>Location</span> <i class="fa fa-caret-down"></i></label>
                            <ul data-id="type" class="typeListing" data-filter-group="location">
                                <li data-filter="" data-value="all" class="selected" data-search="All">
                                    <a href="javascript:void(0)">All</a></li>
                                <li data-filter=".new-york--usa" data-value="1" data-category="new_york-_usa" data-search="New York, USA"><a href="javascript:void(0)">New York, USA</a></li>
                                <li data-filter=".boston--usa" data-value="2" data-category="boston-_usa" data-search="Boston, USA"><a href="javascript:void(0)">Boston, USA</a></li>
                                <li data-filter=".london--uk" data-value="3" data-category="london-_uk" data-search="London, UK"><a href="javascript:void(0)">London, UK</a></li>
                                <li data-filter=".paris--france" data-value="4" data-category="paris-_france" data-search="Paris, France"><a href="javascript:void(0)">Paris, France</a></li>
                                <li data-filter=".australia-and-new-zealand" data-value="5" data-category="australia_and_new_zealand" data-search="Australia and New Zealand"><a href="javascript:void(0)">Australia and New Zealand</a></li>
                                <li data-filter=".asia-pacific" data-value="6" data-category="asia_pacific" data-search="Asia Pacific"><a href="javascript:void(0)">Asia Pacific</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 filters-right pl-4">
                    <label for="search" class="searchtext">Search</label>
                    <div class="input-group alt">
                    <input type="text" class="" id="quicksearch">
                    <span class="input-group-append pl-4">
                        <div class=""><i class="fa fa-search"></i></div>
                    </span>
                    </div>
                </div>
            </div>
        </div>
  </div>
</div>
<div class="our-team-list pt-4">
    <div class="container">
        <?php echo do_shortcode( '[wpteam id="38"]'); ?>
    </div>
</div>
<?php get_footer(); ?>