<?php
get_header();

$id = get_the_ID();
$page = get_post($id);

if ($page instanceof WP_Post) { // Check if $page is a valid WP_Post object
    ?>
    <section class="bg bg-white <?=strtolower($page->post_title)?>">
        <div class="container no-pad-gutters">
            <div class="row">
                <div class="col-12 col-md-10">
                    <h1 class="pb-4 text-uppercase"><?php echo $page->post_title ?></h1>
                    <h4 class="normal-font-regular"><?php echo $page->post_content ?></h4>
                </div>
            </div>
        </div>
    </section>
    <?php
} else {
    // Handle the case where $page is not a valid WP_Post object
    echo "Error: Unable to retrieve page data.";
}

get_footer();
?>
